# SADE
SADE, an acronym for «Scalable Architecture for Digital Editions», is a software
package that includes many components, which are particularly suitable for
developing and displaying a digital edition. It focuses on the presentation of
XML documents that follow the guidelines of the Text Encoding Initiative (TEI).
Since 2012 a larger developer community has constantly been working on this
software, which was originally developed at the Berlin-Brandenburg Academy of
Sciences and Humanities (BBAW) as part of the digitization initiative «The
Electronic Life Of The Academy» (TELOTA). Contributors are among others the
BBAW, the Austrian Academy of Sciences (OeAW; as part of the project CLARIN:
Common Language Resources and Technology Infrastructure), the Max Planck
Institute for the History of Science (MPI-WG), the Cologne Center for
eHumanities (CCEH) and the Göttingen State and University Library (SUB; as
part of the project TextGrid).

The software is based on the XML database [eXist](https://exist-db.org) and its
up to date version itself is a customization. It was developed for the TextGrid
infrastructure. Over the years a few components became obsolete, so the current
version is a completely rewritten one.

## Features
- [faceted search](faceted-search.md)
- [TextGrid clients](tgclients.md)
  - transfer data from the TextGrid Repository
  - validation against RelaxNG schema on publish
  - store data in the TextGrid Repository
- [fork/download SADE projects](forking.md)
- [preconfigured indexes](index-configuration.md)
  - Lucene string analyzer
    - configured for TEI
    - easy (as in easy) configuration/maintenance of charmaps and synonyms
  - Range Index
- frontend
  - easy (as in easy) to configure menu bar
  - professional template
- [wiki parser](wikiParser.md)
  - synchronize selected wiki pages
  - [Confluence](confluenceParser.md)
  - [Dokuwiki](dokuwikiParser.md)
- multi-language support
- [prepared for optionally prerendering](prerendering.md)

### Third party addons
- SemToNotes
- TEI stylesheets (XSLT)
- Markdown parser

By default the eXist database offers a RESTful interface to all of your data,
but with the help of a little XQuery, you can provide on-the-fly transformations
via REST as well.

## TextGrid
This version is prepared to present data transferred via the [TextGrid](https://textgrid.de/)
Laboratory. You have to use the SADE-Publisher Plug-In in the Lab.

## Development

Get an overview of the complete architecture and an entry point to start
developing right now at the [Developers Guide](develop.md).

You can find our git repositories at
[GWDG's Gitlab](https://gitlab.gwdg.de/SADE/).

We are happy to get your bug reports and feature requests!
