xquery version "3.1";

import module namespace config="https://sade.textgrid.de/ns/config" at "../config.xqm";

declare namespace tg="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace digili="http://textgrid.info/namespaces/digilib";

let $id := request:get-parameter("id", "")
return
    if( $id = "" )
    then error( QName("", "DIGILI01"), "Got no ID." )
    else
let $reqUrl := config:get("textgrid.digilib") || $id || "/full/" || config:get("textgrid.digilib.defaultSize") ||"/0/default.jpg"
let $header := <headers><header name="Connection" value="close"/></headers>
let $result := httpclient:get( xs:anyURI($reqUrl), false(), $header )
let $mime := xs:string($result//httpclient:header[@name="Content-Type"]/@value)
let $last-modified := xs:string($result//httpclient:header[@name="Last-Modified"]/@value)
let $cache-control := xs:string($result//httpclient:header[@name="Cache-Control"]/@value)
return
    response:stream-binary(xs:base64Binary($result//httpclient:body), $mime)
