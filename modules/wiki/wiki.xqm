xquery version "3.1";
(:~
 : A Module for getting and preparing wikipages from Dokuwiki.
:)

module namespace wiki="http://textgrid.de/ns/SADE/wiki";
import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";

declare namespace http="http://expath.org/ns/http-client";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare function wiki:dokuwiki($node as node(), $model as map(*), $id as xs:string) as node()* {
(: replace id not in whitelist :)
let $id:= if((config:get("dokuwiki.whitelist", "wiki")//text())[.=$id]) then $id else config:get("dokuwiki.whitelist", "wiki")//text())[1]
(: trigger image reload :)
let $reload := if( request:get-parameter-names() = "reload" ) then local:dokuwikiImageReload($id, $model) else false()
(: test 4 new edits in DokuWiki :)
let $lastRevinDb := local:dokuwikiGetLastRevInDb($id)
let $revisionInWiki := local:dokuwikiNewModification($id, $model, $lastRevinDb)

let $doc :=
    if( $revisionInWiki = $lastRevinDb )
    then
        doc('/sade-projects/textgrid/data/xml/doku/' || $id || '.rev' || local:dokuwikiGetLastRevInDb($id) || '.xml' )/div/*
    else
        let $url:= xs:anyURI(config:get("dokuwiki.url") || '/doku.php?id='||config:get("space", "dokuwiki")||':'|| $id || '&amp;rev='|| $revisionInWiki ||'&amp;u='|| config:get("dokuwiki.user") ||'&amp;p='|| config:get("dokuwiki.password") ||'&amp;do=export_xhtml')
        let $persist := false()
        let $request-headers:= ()
        let $options:= <options><property name="http://cyberneko.org/html/properties/default-encoding" value="UTF-8"/></options>
        let $export := ( httpclient:get($url, $persist, $request-headers, $options)//div[@class="dokuwiki export"]
                        , httpclient:get($url, $persist, $request-headers, $options)//DIV[@class="dokuwiki export"] )
        let $result := if( $export//DIV ) then local:HTML2html($export) else $export
        let $result := <div>{ local:dokuwikiparser( $result/*[not(@id='dw__toc')] ) }</div>
        let $login := xmldb:login( '/sade-projects/textgrid/data/xml/doku/', config:get('sade.user') , config:get("sade.password"))
        let $store := xmldb:store( '/sade-projects/textgrid/data/xml/doku/', $id || '.rev' || $revisionInWiki || '.xml' , $result )
        let $imageReload := local:dokuwikiImageReload($id, $model)
            return
                $result
return
    <div id="dokuwiki">{ $doc }</div>
};

declare function local:dokuwikiGetLastRevInDb($id) {
   for $item in xmldb:get-child-resources('/sade-projects/textgrid/data/xml/doku/')
   where substring-before($item, '.rev') = $id
   return
       substring-before(substring-after($item, '.rev'), '.xml')
};

declare function local:dokuwikiNewModification($id, $model, $lastRevinDb) as xs:string {
let $wikiList := doc( config:get("dokuwiki.url")||'/feed.php?type=rss2&amp;num=500&amp;ns=fontane&amp;minor=1&amp;mode=list&amp;u='|| config:get("dokuwiki.user") ||'&amp;p='|| config:get("dokuwiki.password"))
let $lastRevinWiki :=  $wikiList//link[starts-with(., config:get("dokuwiki.url") || '/doku.php?id=fontane:'|| $id ||'&amp;rev=')]/substring-after(., '&amp;rev=')

let $test :=
    if( $lastRevinDb != $lastRevinWiki )
    then
        (
            xmldb:login( '/sade-projects/textgrid/data/xml/doku/', config:get('sade.user'), config:get("sade.password")),
            xmldb:remove('/sade-projects/textgrid/data/xml/doku/', $id || '.rev' || $lastRevinDb || '.xml' )
        )
    else ()
return
    $lastRevinWiki
};

declare function local:HTML2html($node) {
for $node in $node
return
    typeswitch ( $node )
    case element() return element { lower-case($node/local-name()) } { $node/@*, local:HTML2html($node/node()) }
    default return $node
};

declare function local:dokuwikiparser($nodes as node()*){
    local:dokuwikiparser($nodes, false())
};
declare function local:dokuwikiparser($nodes as node()*, $char as xs:boolean){
for $node in $nodes return
    typeswitch($node)
        case element(h1)
        return
            element xhtml:h2  {
                <div class="block-header">
                    <h2><span class="title">{ local:dokuwikiparser($node/text(), $char) }</span><span class="decoration"></span><span class="decoration"></span><span class="decoration"></span></h2>
                    <div id="toggleToc"><i class="fas fa-caret-down" aria-hidden="true"></i></div>
                    <div id="dokuToc">
                        { local:dokuwikiTOC( $node/parent::div/div[@id="dw__toc"]/div/ul/li/ul) }
                    </div>
                </div>
            }
        case element(h2)
        return
            element xhtml:h3  {
                $node/@id,
                <span class="dokuLink"><a href="#{$node/@id}"> <i class="fas fa-link" aria-hidden="true"></i></a></span>,
                local:dokuwikiparser($node/node(), $char)
            }
        case element(h3)
        return
            element xhtml:h4  {
                $node/@id,
                <span class="dokuLink"><a href="#{$node/@id}"> <i class="fas fa-link" aria-hidden="true"></i></a></span>,
                local:dokuwikiparser($node/node(), $char)
            }
        case element(h4)
        return
            element xhtml:h5  {
                $node/@id,
                <span class="dokuLink"><a href="#{$node/@id}"> <i class="fas fa-link" aria-hidden="true"></i></a></span>,
                local:dokuwikiparser($node/node(), $char)
            }
        case element(h5)
        return
            element xhtml:h6  {
                $node/@id,
                <span class="dokuLink"><a href="#{$node/@id}"> <i class="fas fa-link" aria-hidden="true"></i></a></span>,
                local:dokuwikiparser($node/node(), $char)
            }
        case element(h6)
        return
            element xhtml:h7  {
                $node/@id,
                <span class="dokuLink"><a href="#{$node/@id}"> <i class="fas fa-link" aria-hidden="true"></i></a></span>,
                local:dokuwikiparser($node/node(), $char)
            }
        case attribute(class) return ()
        case element(div)
        return
            element xhtml:div {(
                if($node/@class = "noteimportant")
                then
                    attribute class { 'note' }
                else (),
                local:dokuwikiparser($node/node(), $char)
            )}
        case element (table)
        return
            element xhtml:table {
                attribute class {'table'},
                local:dokuwikiparser($node/node(), $char)
            }
        case element (thead)
        return
            element xhtml:thead {
                local:dokuwikiparser($node/node(), $char)
            }
        case element (th)
        return
            element xhtml:th {
                local:dokuwikiparser($node/node(), $char)
            }
        case element (tr)
        return
            element xhtml:tr {
                local:dokuwikiparser($node/node(), $char)
            }
        case element (tbody)
        return
            element xhtml:tbody {
                local:dokuwikiparser($node/node(), $char)
            }
        case element (td)
        return
            element xhtml:td {
                local:dokuwikiparser($node/node(), $char)
            }
        case element(a)
        return
            if(exists($node/img)) then local:dokuwikiparser($node/node(), $char) else
            element xhtml:a {
                attribute href {
                    if($node/@class = "urlextern") then
                    (: wiki external links, should not link to test instance :)
                        attribute href {replace($node/@href, 'fontane-nb.dariah.eu/test/', 'fontane-nb.dariah.eu/')}
                    (: internal links :)
                    else if(starts-with($node/@href, '#'))
                        then string($node/@href)
                        else '?id='||substring-after($node/@href, 'fontane:')},
                $node/@rel,
                $node/@id,
                local:dokuwikiparser($node/node(), $char)
            }
        case element(p)
        return
            element xhtml:p {
                local:dokuwikiparser($node/node(), $char)
            }
        case element (ul)
        return
            element xhtml:ul {
                local:dokuwikiparser($node/node(), $char)
            }
        case element (li)
        return
            element xhtml:li {
                local:dokuwikiparser($node/node(), $char)
            }
        case element (pre)
        return
            element xhtml:pre {
                element xhtml:code {
                    attribute class {'html'},
                    local:dokuwikiparser($node/node(), $char)
                }
            }
        case element (em)
        return
            element xhtml:em {
                if($node/@class = 'u') then attribute class {'underline'} else (),
                local:dokuwikiparser($node/node(), $char)
            }
        case element (strong)
        return
            element xhtml:strong {
                local:dokuwikiparser($node/node(), $char)
            }
        case element (del)
        return
            element xhtml:del {
                local:dokuwikiparser($node/node(), $char)
            }
        case element (img)
        return
            (element xhtml:img {
                $node/@alt,
                $node/@width,
                $node/@title,
                attribute class {'imgLazy'},
                attribute data-original { 'public/doku/' || $node/substring-after(@data-original, 'fontane:') },
                attribute src {"public/img/loader.svg"}
            }, () )
        case text() return
            (: Circa 1,2 Mio mal wird hier replace() aufgerufen. :)
                let $map := map {
                    'Ã¶' : 'ö',
                    'Ã¤' : 'ä',
                    'Ã¤' : 'Ä',
                    'Ã¼' : 'ü',
                    'Ãœ' : 'Ü',
                    'ÃŸ' : 'ß',
                    'â€ž': '„',
                    'â€œ': '“',
                    'âœ“': '“',
                    'Å¿' : 'ſ',
                    'â†¯': '↯',
                    'â€¢': '',
                    'â€“': '–',
                    'â€”': '—',
                    'âˆ’': '−',
                    'â€¦': '…',
                    'Ã–' : 'Ö',
                    'â¸—': '⸗',
                    'â€˜': '‘',
                    'â€š': ',',
                    'Ã©' : 'é',
                    'â€™': '’',
                    'â•’': '╒',
                    'â€º': '›',
                    'â€¹': '‹',
                    'Ã«' : 'ë',
                    'Ã´' : 'ô',
                    'Ã¯' : 'ï',
                    'á¸¯': 'ḯ',
                    'Ã¿' : 'ÿ',
                    'Â½' : '½',
                    'Â°' : '°'
                }
                return
                    if($char) then $node else
                    local:asciiutf8($node, $map)
        default return local:dokuwikiparser($node/node(), $char)
(:        case text() return $node:)
(:        default return local:dokuwikiparser($node/node()):)
};

declare function local:dokuwikiTOC($nodes) {
for $node in $nodes return
    typeswitch($node)
    case element(div) return
        local:dokuwikiTOC($node/node())
    case element() return
        element { $node/local-name() } { $node/@*[local-name() != 'class'], local:dokuwikiTOC($node/node()) }
    case text() return
        let $map := map {
            'Ã¶' : 'ö',
            'Ã¤' : 'ä',
            'Ã¤' : 'Ä',
            'Ã¼' : 'ü',
            'Ãœ' : 'Ü',
            'ÃŸ' : 'ß',
            '¶ÃŸ': 'ß',
            'â€ž': '„',
            'â€œ': '“',
            'âœ“': '“',
            'Å¿' : 'ſ',
            'â†¯': '↯',
            'â€¢': '',
            'â€“': '–',
            'â€”': '—',
            'âˆ’': '−',
            'â€¦': '…',
            'Ã–' : 'Ö',
            'â¸—': '⸗',
            'â€˜': '‘',
            'â€š': ',',
            'Ã©' : 'é',
            'â€™': '’',
            'â•’': '╒',
            'â€º': '›',
            'â€¹': '‹',
            'Ã«' : 'ë',
            'Ã´' : 'ô',
            'Ã¯' : 'ï',
            'á¸¯': 'ḯ',
            'Ã¿' : 'ÿ',
            'Â½' : '½',
            'Â°' : '°',
            '' : ''
        }
        return
            local:asciiutf8($node, $map)
    default return local:dokuwikiTOC($node/node())
};

declare function local:asciiutf8($text as text(), $map as map()) as text(){

let $pattern := map:keys($map)[1]
let $replacement := map:get( $map, $pattern )

let $newText := text { replace($text, $pattern, $replacement) }

let $newMap := map:remove( $map, $pattern )

return
    if(count(map:keys($newMap)) gt 0) then  local:asciiutf8($newText, $newMap)
    else $newText

};

declare function local:dokuwikiImageReload($id, $model) {
let $path := system:get-exist-home() || '/tools/jetty/webapps/portal/public/doku/'
let $credentials := '&amp;u='|| config:get("dokuwiki.user") ||'&amp;p='|| config:get("dokuwiki.password")
let $url:= xs:anyURI(config:get("url", "dokuwiki")||'/doku.php?id='||config:get("space", "dokuwiki")||':'|| $id ||'&amp;do=export_xhtml' || $credentials)
let $persist := false()
let $request-headers:= <http:header name="Accept-Charset" value="UTF-8"/>
let $result:= httpclient:get($url, $persist, $request-headers)//div[@class="dokuwiki export"]/*[not(@id='dw__toc')]
let $images := distinct-values( $result//img/substring-after(@data-original, 'fontane:') )
return
    for $image in $images
    return
    let $id := $image
    let $req := <http:request href="{config:get("dokuwiki.url") ||"/lib/exe/fetch.php?media="||config:get("space", "dokuwiki")||":"|| $id || $credentials }" method="get">
                <http:header name="Connection" value="close"/>
            </http:request>
    let $result := http:send-request($req)
    let $mime := xs:string($result[1]//http:header[@name="content-type"]/@value)
    let $last-modified := xs:string($result[1]//http:header[@name="last-modified"]/@value)
    let $cache-control := xs:string($result[1]//http:header[@name="cache-control"]/@value)
    let $image := xs:base64Binary($result[2])
    let $login:= xmldb:login( '/sade-projects/textgrid/data/xml/doku/', config:get('sade.user') , config:get("sade.password"))
    let $store := if( file:exists($path || $id)) then () else file:serialize-binary($image, $path || $id)
    return
        ()
};
