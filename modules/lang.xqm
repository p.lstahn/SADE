xquery version "3.1";

module namespace lang="https://sade.textgrid.de/ns/lang";

import module namespace app="https://sade.textgrid.de/ns/app" at "app.xql";
import module namespace functx="http://www.functx.com";
import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";
import module namespace console="http://exist-db.org/xquery/console";

declare namespace lf="https://sade.textgrid.de/ns/langfile";

declare variable $lang:lang := app:getLanguage();
declare variable $lang:allLanguages := app:getAllLanguages();
declare variable $lang:langEnabled := string(config:get("lang.enabled", "multilanguage"));

declare function local:switchLanguage($to) as xs:string {
    let $query-string := (
    tokenize(
        request:get-query-string(), "&amp;"
    )[not(starts-with(., "lang="))],
    "lang="||$to)
    return "?" || string-join($query-string, "&amp;")
};

declare function local:multiLangSwitcher($langconfig, $lang) as node(){
    let $test := "test"
    return element a { 
            attribute href {local:switchLanguage($lang)},
            $langconfig//lf:word[@key="Language"]/lf:lang[@key=$lang]
        }
};

declare function lang:translate( $node as node(), $model as map(*), $content as xs:string ) as node() {

let $langconfig := doc( $config:app-root || "/lang.xml" )
return
    typeswitch($node)
        case element (span) return element {name($node)} {
            $langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]
        }
        case element (input) return element input {
            (: Language stuff for search in results.html :)
            if ($content = "SearchResults") then (
                attribute id {"searchLang"},
                attribute type {"hidden"},
                attribute name {"lang"},
                attribute value {$lang:lang}
            )
            (: Placeholder stuff for input items :)
            else (
                attribute placeholder {$langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]},
                for $att in $node/@*
                    let $att-name := name($att)
                    return if ($att-name != "placeholder") then attribute {$att-name} {$att}
                    else ()
            )
        }
        case element (textarea) return element textarea {
            attribute placeholder {$langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]},
            for $att in $node/@*
                let $att-name := name($att)
                return if ($att-name != "placeholder") then attribute {$att-name} {$att}
                else ()
        }
        case element (button) return element button {
            for $att in $node/@*
                let $att-name := name($att)
                return if ($att-name != "placeholder") then attribute {$att-name} {$att}
                else (),
            $langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]
        }
        case element (li) return element li {
            if ($content = "multilang") then (
                if ($lang:langEnabled = "true") then (
                    attribute class {"dropdown"},
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding-top:0px;padding-bottom:0px;height:55px">
                        <i class="fas fa-language fa-3x" style="position:relative;top:15%"/>
                            <span class="caret"/>
                    </a>,
                    <ul class="dropdown-menu">
                        <li>
                            {for $el in $lang:allLanguages return (local:multiLangSwitcher($langconfig,$el))}
                        </li>
                    </ul>

                    )
                else ()
            )
            else ()
        }
        default return element {name($node)} {}
};
